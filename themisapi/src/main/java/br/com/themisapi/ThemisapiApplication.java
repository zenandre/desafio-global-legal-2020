package br.com.themisapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThemisapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThemisapiApplication.class, args);
	}

}
