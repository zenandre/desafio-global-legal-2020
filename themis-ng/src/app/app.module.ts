import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { DetalheIpComponent } from './detalhe-ip/detalhe-ip.component';
import { InqueritosComponent } from './inqueritos/inqueritos.component';
import {NgbModule}from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import LocalePt from '@angular/common/locales/pt'
import { registerLocaleData } from '@angular/common';
import { NgxSpinnerModule } from "ngx-spinner";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'

registerLocaleData(LocalePt)

@NgModule({
  declarations: [
    AppComponent,
    DetalheIpComponent,
    InqueritosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    NgxSpinnerModule,
    BrowserAnimationsModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'pt'}],
  bootstrap: [AppComponent]
})
export class AppModule { }
