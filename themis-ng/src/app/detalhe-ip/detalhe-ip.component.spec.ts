import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalheIpComponent } from './detalhe-ip.component';

describe('DetalheIpComponent', () => {
  let component: DetalheIpComponent;
  let fixture: ComponentFixture<DetalheIpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalheIpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalheIpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
