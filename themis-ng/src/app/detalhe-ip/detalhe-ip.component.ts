import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-detalhe-ip',
  templateUrl: './detalhe-ip.component.html',
  styleUrls: ['./detalhe-ip.component.sass']
})
export class DetalheIpComponent implements OnInit {

  inquerito: any;

  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private modalService: NgbModal,
    private router: Router
  ) { }

  ngOnInit() {
    this.http.get(`http://localhost:3000/inqueritos?id=${this.route.snapshot.params.id}`).subscribe(
      ip => this.inquerito = ip[0]
    )    
  }
  openModalAcordo(content) {
    this.modalService.open(content, { size: 'xl' });
  }

  openModalCalibrarIa(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  salvarAlteracoes(){
    if(this.inquerito.textoCalibracaoIa != null &&
      this.inquerito.textoCalibracaoIa != undefined &&
      this.inquerito.textoCalibracaoIa != ""){
        this.inquerito.elegivelParaAcordo = "NAO"
      } else if(this.inquerito.elegivelParaAcordo == "SIM") {
         this.inquerito.elegivelParaAcordo = "SIM"
        } else {this.inquerito.elegivelParaAcordo = ""}

      this.http.put(`http://localhost:3000/inqueritos/${this.inquerito.id}`,this.inquerito).subscribe(
        ip => { 
          if(ip){
             this.router.navigate([''])
            }
          }
      )


  }



}
