import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-inqueritos',
  templateUrl: './inqueritos.component.html',
  styleUrls: ['./inqueritos.component.sass']
})
export class InqueritosComponent implements OnInit {

  inqueritos: any[];

  constructor(
    private http: HttpClient,
    private spinner: NgxSpinnerService
  ){

  }
 
  getInqueritos(){
    return this.http.get('http://localhost:3000/inqueritos');
  }

  ngOnInit(){


    this.getInqueritos().subscribe(
      ips => { 
        console.log(ips)
        //@ts-ignore
        this.inqueritos = ips;
      }
    )

  }

  processarInqueritos(){
    this.spinner.show();
    setTimeout(
      () => {
        this.spinner.hide();
        this.inqueritos.map(
          ip => {
            if (
              ip.tempoMinimo <= 4 &&
              ip.reuPrimario == true &&
              ip.semGraveAmeaca == true
            ){
              ip.elegivelParaAcordo = "SIM"
            } else { ip.elegivelParaAcordo = "NAO" }
            this.http.put(`http://localhost:3000/inqueritos/${ip.id}`,ip).subscribe()
          }
        )
      }, 8000
    )
    
  }

  carregarInqueritosNaoProcessados(){
    this.spinner.show();
    this.getInqueritos().subscribe(
      ips => {
        this.spinner.hide();
        //@ts-ignore
        ips.map( ip => ip.elegivelParaAcordo = "")
        //@ts-ignore
        this.inqueritos = ips;
      }
    )
  }

}
