import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetalheIpComponent } from './detalhe-ip/detalhe-ip.component';
import { InqueritosComponent } from './inqueritos/inqueritos.component';


const routes: Routes = [
  {path: '',component: InqueritosComponent},
  {path: 'detalhe/:id',component: DetalheIpComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
