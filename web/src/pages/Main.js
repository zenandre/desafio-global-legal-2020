import React, {useState,useEffect} from 'react';
import axios from 'axios';

import './Main.css'


export default function Main(){

    const [inqueritos,setInqueritos] = useState([]);

    useEffect(
        async () => { 
            const ips = await axios.get('/localhost:3000/inqueritos')
            console.log(ips)
            setInqueritos(ips)
        },
        []
    )

    return(
        <div>
            <h1>Sistema de Acordo de Não Persecução Penal - MPCE</h1>
            <ul>
                {
                    inqueritos.map(
                        ip => (
                            <li key={ip.id}>
                                {ip.numero}
                            </li>
                        )
                    )
                }
            </ul>
        </div>
    )
}
